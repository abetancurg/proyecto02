
# """ desde flask se debe importar request para que a traves del metodo POST los datos que
# incorporemos sean recibidos y podamos visualizarlos en la consola o terminal y poder chequear
# que efectivamente los valores esten viajando. se debe importar uuid para generar id aleatorios
# con el fin de que el server envie una respuesta al browser con un id aleatorio una vez se haya 
# almacenado los datos enviados al server. Recordar que se usar print() cuando deseamos ver
# los datos enviados por la consola, y return cuando los datos los queremos ver en el browser """

from flask import Flask, request
#import uuid
from marshmallow import Schema, fields, ValidationError, validates   #Para que funcione se debe instalar previamente
from flask_smorest import Api, Blueprint, abort
from flask.views import MethodView
from datetime import datetime #now
import uuid



app = Flask(__name__)

#-------------------------------------------------intento swagger--------------------------------------------------------
# app.config['API_TITLE'] = 'My API'
# app.config['API_VERSION'] = 'v1'
# app.config['OPENAPI_VERSION'] = '3.0.2'
# api = Api(app)


# class PetSchema(Schema):
#     id = fields.Int()
#     nombre = fields.String()

# class PetQueryArgsSchema(Schema):
#     name = fields.String()

# blp = Blueprint(
#     'pets', 'pets', url_prefix='/pets',
#     description='Operations on pets'
# )

# @blp.route('/prueba')
# class Pets(MethodView):

#     @blp.arguments(PetQueryArgsSchema, location='query')
#     @blp.response(PetSchema(many=True))
#     def get(self, args):
#         """List pets"""
#         return Pet.get(filters=args)
#-----------------------------------------------marshmallow con inputs----------------------------------------------------------

# class PersonSchema(Schema):
#     name = fields.String()
#     email = fields.Email()
#     fecha = fields.DateTime(format='%Y-%m-%d')

# input_data = {}

# input_data['name'] = input('Dime tu nombre? ')
# input_data['email'] = input('Digita tu email? ')
# input_data['fecha'] = input('Dinos la fecha de hoy en formato AAAA-MM-DD? ')

# schema = PersonSchema()
# person = schema.load(input_data)  #deserializo el dict
# result = schema.dump(person)  #serializo el dict
# print(result)



# ------------------------ clase imolko marshmallow usando un browser y consola 2020-07-28----------------------------------------------

def calcular_rango_edad(record):
    print (f"en calcular rango edad: {record}")
    edad = record.get("edad")
    if edad < 25:
        return "jovencito"
    if edad >= 25 and edad < 70:
        return "adulto"
    return "viejito"

class PersonSchema(Schema):
        nombre = fields.Str()
        apellido = fields.Str()
        fecha = fields.DateTime(format='%Y-%m-%d')
        created = fields.DateTime(format='%Y-%m-%d', default=datetime.now)                                                 #sino viene aplicar missing..dump_only=True
        edad = fields.Integer(required=True)
        idkey = fields.UUID(dump_only=True, default=uuid.uuid4)
        rango_edad = fields.Function(serialize=calcular_rango_edad)

        @validates("edad")
        def validar_edad(self, edad):
            if edad > 100:
                raise ValidationError("No se aceptan vejucos")

@app.route("/")
def hello_www():

    input_data = {'created':'2019-01-01','edad':70,'nombre':'andres', 'apellido':'Betancur', 'fecha': '1980-12-17'}

# input_data['name'] = input('Dime tu nombre? ')
# input_data['email'] = input('Digita tu email? ')
# input_data['fecha'] = input('Dinos la fecha de hoy en formato AAAA-MM-DD? ')

    schema = PersonSchema()
    print(f"input_data:{input_data}")
    person = schema.load(input_data)  #deserializo el dict
    print(f"loaded:{person}")
    result = schema.dump(person)      #serializo el dict
    print(f"dumpeado:{result}")
    return(result)

#-----------------------------------------clase imolko endpoints 2020-07-24----------------------------------------------------------------


# """ a pesar que sabemos a continuacion que la variable lista o array llamada lista contiene
#  unos datos, estos deben de castearse a str para que el ojo humano pueda identificarlos, de
#  lo contrario se nos puede generar un error tipo callable"""

# @app.route("/seguimientos", methods=['GET'])
# def get_seguimiento():
#     lista = [
#         {"Descripcion":"Llamada exitosa","Fecha":"20200717","Hora":"1400"},
#         {"Descripcion":"Llamada exitosa","Fecha":"20200717","Hora":"1800"},
#         {"Descripcion":"Llamada exitosa","Fecha":"20200717","Hora":"1900"},
#         {"Descripcion":"Llamada exitosa","Fecha":"20200717","Hora":"2000"}
#     ]
#     return str(lista), 200

#  """ se adiciona, 201 para que en la consola la respuesta de datos venga acompanada con los 
#  codigos convencionales de respuesta"""

# @app.route("/seguimientos", methods=['POST'])
# def post_seguimiento():
#     print(request.form) 
#     seguimiento_id = str(uuid.uuid4())
#     return seguimiento_id, 201

# """ se usa <variable> como variable que recibe la descripcion del ultimo nivel de la url que
# se ejecuta en el curl o en el browser directamente, ejemplo: https:127.0.0.1:5000/seguimiento/variable?version=descripcion
# --> <variable>, toma la descripcion variable. La parte luego del ? se conoce como query string y corresponde a las variables
# que viajan al server y para conocerlas se deben invocar con request.args.get :)"""

# @app.route("/seguimientos/<seguimiento_id>", methods=['GET'])
# def get_seguimiento_detail(seguimiento_id):
#     version = request.args.get("version")
#     return "Aqui se mostraria el detalle " + seguimiento_id + "-" + version


# api.register_blueprint(blp)

